
### What is this repository for? ###

Bitirme projesi olarak tamamlanan bu projede Android Destekli Uzaktan Hasta Takibi yapılmaktadır. Python programlama dili ile
Ubidots bulut sistemine erişim sağlanıp hastanın Gyro, Sıcaklık, Nem ve Nabız sensörlerinde okunan değerleri bu sisteme
gönderilmişir. Ubidots verilerinin Android Studio ortamı ile bağlantısı kurularak veriler uygulamaya alınmıştır. 
-Ubidots sistemde sensör bilgileri,
-Firebase sisteminde doktor, hasta ve hasta yakını bilgileri mevcuttur.
-Doktor giriş ekranından kayıtlı olan doktor giriş yaparak hastalarının listesini görebilmektedir. Seçtiği bir hastanın 
sağlık bilgilerini ekranında liste halinde inceleyebilmektedir.
-Hasta yakını giriş ekranından giriş yapılabilmesi için hasta yakınının sistemde kayıtlı olması gerekmektedir. Kayıtlı olmayan 
hasta yakını bilgilerini girerek sisteme kayıt olabilir. Sistemde kaydedilen hasta yakınının ayrıca doktor onayını da alması 
gerekmektedir.
-Sisteme kayıt yapan hasta yakını doktorun onay bekleyenler ekranına düşer. Doktor buradan hasta yakınını onaylar veya siler.
-Onaylanan hasta yakını görmek istediği hastanın bilgilerine, kendi kullanıcı adı ve hastanın Tc kimlik numarası ile ulaşabilir.

Bu projede Restful apiler, Raspberry Pi3, bahsedilen sensörler, Json datalar ve Http Protokolleri kullanılmıştır.