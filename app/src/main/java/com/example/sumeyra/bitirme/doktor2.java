package com.example.sumeyra.bitirme;

/**
 * Created by Sumeyra on 1.4.2017.
 */
public class doktor2 {
        private String ad, soyad, tc, token, id;
    public doktor2() {
    }
        public doktor2(String ad, String soyad, String tc, String token, String id) {
            this.ad = ad;
            this.soyad = soyad;
            this.tc=tc;
            this.token = token;
            this.id = id;
        }

        public String getAd() {
            return ad;
        }

        public void setAd(String ad) {
            this.ad = ad;
        }

        public String getSoyad() {
            return soyad;
        }

        public void setSoyad(String soyad) {
            this.soyad = soyad;
        }

        public String getTc() {
            return tc;
        }

        public void setTc(String tc) {
            this.tc = tc;
        }

    public String getToken() { return token; }

    public void setToken(String token) {this.token = token;}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

