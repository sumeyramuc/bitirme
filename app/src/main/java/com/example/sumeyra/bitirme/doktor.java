package com.example.sumeyra.bitirme;

/**
 * Created by elf on 12.3.2017.
 */
public class doktor {

    private String ad, soyad, tc;

    public doktor() {
    }

    public doktor(String ad, String soyad, String tc) {
        this.ad = ad;
        this.soyad = soyad;
        this.tc=tc;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }

}


