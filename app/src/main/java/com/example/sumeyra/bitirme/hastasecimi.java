package com.example.sumeyra.bitirme;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Sumeyra on 6.2.2017.
 */
public class hastasecimi extends ActionBarActivity {
    FirebaseDatabase db = FirebaseDatabase.getInstance();
    TableLayout tableLayout;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hastasecimi);

        tableLayout = (TableLayout) findViewById(R.id.tablelayout);

        DatabaseReference dbGelenler = db.getReference("doktor").child("hastabilgi");
        dbGelenler.addListenerForSingleValueEvent(new ValueEventListener() {

            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot gelenler : dataSnapshot.getChildren()) {

                    String ad = gelenler.getValue(doktor2.class).getAd();
                    String soyad = gelenler.getValue(doktor2.class).getSoyad();
                    String token = gelenler.getValue(doktor2.class).getToken();
                    String id = gelenler.getValue(doktor2.class).getId();
                    listele(ad, soyad, token,id);


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });

}
    private void listele(String ad, String soyad, final String token, final String id)
    {
        Button hastaadi=new Button(this);
        hastaadi.setText(ad + " " + soyad);
        hastaadi.setPadding(15,15,15,15);
        tableLayout.addView(hastaadi);
        hastaadi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(hastasecimi.this, hastabilgileri.class);
                i.putExtra("gidenVeri", token);
                i.putExtra("gidenVeri2", id);
                startActivity(i);

            }
        });
    }
}