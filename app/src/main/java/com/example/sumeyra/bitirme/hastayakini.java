package com.example.sumeyra.bitirme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Sumeyra on 27.3.2017.
 */
public class hastayakini extends AppCompatActivity implements View.OnClickListener{

    FirebaseDatabase db,db2;
    EditText editText, editText2;
    private Button girisyap,kaydol;
    private TextView kayitlar,token;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hastayakini);

        baslangic();
    }

    private void baslangic() {

        editText = (EditText) findViewById(R.id.ad);
        editText2 = (EditText) findViewById(R.id.soyad);
        db = FirebaseDatabase.getInstance();
        db2 = FirebaseDatabase.getInstance();
        girisyap = (Button) findViewById(R.id.girisyap);
        kaydol = (Button) findViewById(R.id.kaydol);
        kayitlar = (TextView) findViewById(R.id.ad);
        token=(TextView) findViewById(R.id.textView);

        girisyap.setOnClickListener(this);
        kaydol.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.girisyap:
                kayitlariGetir();
                break;
            case R.id.kaydol:
                Intent i = new Intent();
                i.setClass(hastayakini.this, kaydol.class);
                startActivity(i);
                break;
        }
    }

    private void kayitlariGetir() {
        if(editText.getText().toString().equals("") || editText2.getText().toString().equals(""))
        {AlertDialog alertMessage = new AlertDialog.Builder(this).create();
            alertMessage.setTitle("UYARI");
            alertMessage.setMessage("Boş alanları doldurunuz!");
            alertMessage.show();}
        else {
            final DatabaseReference dbGelenler = db.getReference("hastayakini");
            final DatabaseReference dbGelenler2 = db2.getReference("doktor").child("hastabilgi");
            dbGelenler.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot gelenler : dataSnapshot.getChildren()) {
                        if (gelenler.getValue(doktor.class).getAd().equals(editText.getText().toString())) {
                            if (gelenler.getValue(doktor.class).getTc().equals(editText2.getText().toString())) {
                                dbGelenler2.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot2) {
                                        for (DataSnapshot gelenler2 : dataSnapshot2.getChildren()) {
                                            if (gelenler.getValue(doktor.class).getTc().equals(gelenler2.getValue(doktor2.class).getTc())) {
                                                String tkn = gelenler2.getValue(doktor2.class).getToken();
                                                String id = gelenler2.getValue(doktor2.class).getId();
                                                Intent i = new Intent();
                                                i.setClass(hastayakini.this, hastabilgileri.class);
                                                i.putExtra("gidenVeri", tkn);
                                                i.putExtra("gidenVeri2", id);
                                                startActivity(i);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            } else
                                Toast.makeText(getApplicationContext(), "kullanici adi veya sifre hatali", Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(getApplicationContext(), "kullanici adi veya sifre hatali", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }


            });
        }

    }
}




