package com.example.sumeyra.bitirme;
//bitirme projesi

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class hastabilgileri extends ActionBarActivity {

    private String gelenVeri, gelenVeri2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hastabilgileri);

        Intent i = getIntent();
        this.gelenVeri= i.getStringExtra("gidenVeri").toString();
        this.gelenVeri2=i.getStringExtra("gidenVeri2").toString();

        String id = this.gelenVeri2;
        String token = this.gelenVeri;

        String link = "?token="+token;
        link=id+"/variables/"+link;
        String resultt = httpgetpost(link);
        response(resultt);
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        inputStream.close();
        return result;
    }

    public String httpgetpost(String link) {
        final String url = "http://things.ubidots.com/api/v1.6/datasources/" + link;
        InputStream inputStream = null;
        String result1 = "";

        try {

            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                //içeriği input stream` e eşitliyoruz.
                inputStream = httpResponse.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                //inputStream den string`e dönüşüm
                if (inputStream != null)
                    result1 = convertInputStreamToString(inputStream);
                else
                    result1 = "Did not work!";
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result1;
    }

    public void response(String result) {
        final TableLayout tableLayout = (TableLayout) findViewById(R.id.tablelayout);
        TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        TableRow row2 = new TableRow(this);
        row2.setLayoutParams(lp2);
        row2.setPadding(60, 60, 60, 60);

        TextView Ad=new TextView(this);
        TextView Sensor=new TextView(this);
        TextView Date=new TextView(this);
        Ad.setText("Değer");
        Ad.setPadding(3,3,3,3);
        Ad.setBackgroundColor(Color.parseColor("#337ab7"));
        Ad.setTextColor(Color.parseColor("#ffffff"));
        Sensor.setText("Sensör");
        Sensor.setBackgroundColor(Color.parseColor("#337ab7"));
        Sensor.setTextColor(Color.parseColor("#ffffff"));
        Date.setText("Son Güncelleme");
        Date.setBackgroundColor(Color.parseColor("#337ab7"));
        Date.setTextColor(Color.parseColor("#ffffff"));
        row2.addView(Ad);
        row2.addView(Sensor);
        row2.addView(Date);
        row2.setLayoutParams(lp2);
        tableLayout.addView(row2);

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String date = dateformat.format(new Date());
        int length = 0;
        JSONArray jsonarray = null;
        JSONObject jsonobject = null;
        JSONObject subObject = null;
        try {
            jsonobject = new JSONObject(result);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            jsonarray = jsonobject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            length = jsonobject.getInt("count");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject2 = null;
        for (int i = 0; i < length; i++) {
            try {
                jsonObject2 = jsonarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                subObject = jsonObject2.getJSONObject("last_value");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            TableRow row1 = new TableRow(this);
            TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row1.setPadding(70, 70, 70, 70);
            row1.setLayoutParams(lp1);

            final TableRow row = new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);

            row.setPadding(70, 70, 70, 70);

            row.setLayoutParams(lp);

            try {
                String title = subObject.getString("value");
                TextView text = new TextView(this);
                text.setBackgroundColor(Color.parseColor("#337ab7"));
                text.setTextColor(Color.parseColor("#ffffff"));
                text.setText(title);
                text.setPadding(15, 15, 15, 15);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                System.out.println();
                String status = jsonObject2.getString("name");
                TextView text = new TextView(this);
                text.setBackgroundColor(Color.parseColor("#337ab7"));
                text.setTextColor(Color.parseColor("#ffffff"));
                if(status.equals("GyroX"))
                {
                    int title = subObject.getInt("value");
                    if(title>=0)
                    {
                        if(title<=45)
                            text.setText("HASTA YATAY POZISYONDA");
                    }
                    else
                        text.setText("HASTA AYAKTA");
                }
                else{
                text.setText(status);}
                text.setPadding(15, 15, 15, 15);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                System.out.println();
                long createDate = subObject.getLong("timestamp");
                Calendar calendar = Calendar.getInstance();// tarih içerisinde belli bir günü bir değişkene atamak için Java bizlere Calendar adında bir sınıf sunar. Bu sınıf sayesinde tarih akışı içinde herhangi bir güne ulaşabiliriz.
                calendar.setTimeInMillis(createDate);
                Date currenTimeZone = calendar.getTime();
                TextView text = new TextView(this);
                text.setBackgroundColor(Color.parseColor("#337ab7"));
                text.setTextColor(Color.parseColor("#ffffff"));
                text.setText(dateformat.format(currenTimeZone));
                text.setPadding(15, 15, 15, 15);
                row.addView(text);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            tableLayout.addView(row);

        }
    }
}


