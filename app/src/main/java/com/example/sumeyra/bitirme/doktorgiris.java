package com.example.sumeyra.bitirme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Sumeyra on 3.3.2017.
 */
public class doktorgiris extends AppCompatActivity implements View.OnClickListener {
    FirebaseDatabase db;
    EditText editText, editText2;
    private Button giris;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doktorgiris);

        baslangic();
    }

    private void baslangic() {

        editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        db = FirebaseDatabase.getInstance();
        giris = (Button) findViewById(R.id.giris);

        giris.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.giris:
                kayitlariGetir();
                break;
        }
    }

    private void kayitlariGetir() {
        if(editText.getText().toString().equals("") || editText2.getText().toString().equals(""))
        {AlertDialog alertMessage = new AlertDialog.Builder(this).create();
            alertMessage.setTitle("UYARI");
            alertMessage.setMessage("Boş alanları doldurunuz!");
            alertMessage.show();}
        else {
            final DatabaseReference dbGelenler = db.getReference("doktor");
            dbGelenler.addListenerForSingleValueEvent(new ValueEventListener() {

                public void onDataChange(DataSnapshot dataSnapshot) {

                    Iterable<DataSnapshot> values = dataSnapshot.getChildren();
                    for (DataSnapshot value : values) {
                        if (dataSnapshot.child("kullaniciadi").getValue().toString().equals(editText.getText().toString())) {
                            if (dataSnapshot.child("sifre").getValue().toString().equals(editText2.getText().toString())) {
                                Intent i = new Intent();
                                i.setClass(doktorgiris.this, MainActivity.class);
                                startActivity(i);
                            } else
                                Toast.makeText(getApplicationContext(), "kullanici adi veya sifre hatali", Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(getApplicationContext(), "kullanici adi veya sifre hatali", Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }


            });
        }

    }
}

