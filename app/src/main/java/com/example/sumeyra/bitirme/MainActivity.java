package com.example.sumeyra.bitirme;
//bitirme projesi

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class
        MainActivity extends TabActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
        TabHost.TabSpec tab1 = tabHost.newTabSpec("tab1");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("tab2");

        tab1.setIndicator("Hastalar", getResources().getDrawable(R.drawable.tab)).setContent(new Intent(this, hastasecimi.class));
        tab2.setIndicator("Onay Bekleyenler", getResources().getDrawable(R.drawable.tab)).setContent(new Intent(this, onaybekleyenler.class));

        tabHost.addTab(tab1);
        tabHost.addTab(tab2);

    }
}

