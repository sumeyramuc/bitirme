package com.example.sumeyra.bitirme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Sumeyra on 27.3.2017.
 */
public class kaydol extends AppCompatActivity implements View.OnClickListener {
    FirebaseDatabase db;
    EditText ad, soyad, hastaTC;
    private Button kaydol;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.kaydol);
        baslangic();

    }
    private void baslangic() {

        ad = (EditText) findViewById(R.id.ad);
        soyad = (EditText) findViewById(R.id.soyad);
        hastaTC = (EditText) findViewById(R.id.hastaTC);
        db = FirebaseDatabase.getInstance();
        kaydol = (Button) findViewById(R.id.kaydol);

        kaydol.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.kaydol:
                String isim,soyisim,tc;
                isim=ad.getText().toString().trim();
                soyisim=soyad.getText().toString().trim();
                tc=hastaTC.getText().toString().trim();
                benzerverikontrol(isim,soyisim,tc);
                break;
        }
    }
    private void benzerverikontrol(final String ad, final String soyad, final String tc)
    {
        if(ad.equals("") || soyad.equals("") || tc.equals(""))
        {AlertDialog alertMessage = new AlertDialog.Builder(this).create();
            alertMessage.setTitle("UYARI");
            alertMessage.setMessage("Boş alanları doldurunuz!");
            alertMessage.show();}
        else {
            final DatabaseReference dbGelenler = db.getReference("onay");
            dbGelenler.addListenerForSingleValueEvent(new ValueEventListener() {
                int sayac = 0;

                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot gelenler : dataSnapshot.getChildren()) {
                        if (gelenler.child("ad").getValue().toString().equals(ad) &&
                                gelenler.child("soyad").getValue().toString().equals(soyad) &&
                                gelenler.child("tc").getValue().toString().equals(tc)) {
                            Toast.makeText(getApplicationContext(), "böyle bir kayit sistemde mevcuttur!", Toast.LENGTH_LONG).show();
                            sayac++;
                        }
                    }
                    if (sayac == 0) {
                        kullaniciKaydet(ad, soyad, tc);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    private void kullaniciKaydet(String ad, String soyad, String tc){
        TextView aciklama=new TextView(this);
        aciklama.setText("Hasta Bilgilerini Görme İsteğiniz Onaylandığında Giriş Yapabileceksiniz");
        DatabaseReference dbRef = db.getReference("onay");
        String key = dbRef.push().getKey();
        DatabaseReference dbRefKeyli = db.getReference("onay/"+key);

        dbRefKeyli.setValue(new doktor(ad, soyad, tc));

    }
}
