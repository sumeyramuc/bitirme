package com.example.sumeyra.bitirme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sumeyra on 6.2.2017.
 */
public class onaybekleyenler extends AppCompatActivity {
    FirebaseDatabase db;
    private int selectedPosition = -1;
    int choose = 0;
    private Button onayla,sil;
    TextView gor;
    TableLayout tableLayout;
    CheckBox cb = null;
    final ArrayList<CheckBox> mCheckBoxes = new ArrayList<CheckBox>();

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.onaybekleyenler);
        tableLayout = (TableLayout) findViewById(R.id.tablelayout);
        TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        TableRow row1 = new TableRow(this);
        row1.setLayoutParams(lp1);
        row1.setPadding(60, 60, 60, 60);
        TextView Ad=new TextView(this);
        TextView Soyad=new TextView(this);
        TextView HastaTC=new TextView(this);
        Ad.setText("Ad");
        Soyad.setText("Soyad");
        HastaTC.setText("Hasta TC");
        row1.addView(Ad);
        row1.addView(Soyad);
        row1.addView(HastaTC);
        tableLayout.addView(row1);
        baslangic();

        final DatabaseReference dbGelenler = db.getReference("onay");
        dbGelenler.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot gelenler : dataSnapshot.getChildren()) {
                    textolustur(gelenler);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void baslangic()
    {

        db = FirebaseDatabase.getInstance();
        onayla = (Button) findViewById(R.id.button2);
        sil = (Button) findViewById(R.id.button3);
        onayla.setEnabled(false);
        sil.setEnabled(false);
        gor = (TextView) findViewById(R.id.gor);
    }

    private void textolustur(final DataSnapshot gelenler) {
        final TableRow row = new TableRow(this);

        TableRow.LayoutParams lp2 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setPadding(30, 30, 30, 30);
        row.setLayoutParams(lp2);


        final DatabaseReference refGonder=gelenler.getRef();
        final String ad=gelenler.getValue(doktor.class).getAd();

        TextView textView1 = new TextView(this);
        textView1.setText(ad);
        textView1.setPadding(5, 5, 5, 5);
        row.addView(textView1);

        final String soyad=gelenler.getValue(doktor.class).getSoyad();
        TextView textView2 = new TextView(this);
        textView2.setText(soyad);
        textView2.setPadding(5, 5, 5, 5);
        row.addView(textView2);

        final String tc=gelenler.getValue(doktor.class).getTc();
        TextView textView3 = new TextView(this);
        textView3.setText(tc);
        textView3.setPadding(5, 5, 5, 5);
        row.addView(textView3);

        cb = new CheckBox(getApplicationContext());
        cb.setFocusable(false);
        mCheckBoxes.add(cb);
        cb.setOnClickListener(new View.OnClickListener() {

            //single choice
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    for (int j = 0; j < mCheckBoxes.size(); j++) {
                        if (mCheckBoxes.get(j) == view) {
                            selectedPosition = j;
                            choose = selectedPosition;
                            kontrol(selectedPosition, ad, soyad, tc, refGonder);
                        } else
                            mCheckBoxes.get(j).setChecked(false);
                    }

                } else {
                    selectedPosition = -1;
                }
            }

        });
        row.addView(cb);
        tableLayout.addView(row);


    }

    private void kontrol(int selectedPosition, final String ad, final String soyad, final String tc, final DatabaseReference refGonder) {
        if (selectedPosition == -1) {
            onayla.setEnabled(false);
            sil.setEnabled(false);
        } else {
            onayla.setEnabled(true);
            sil.setEnabled(true);
        }
        onayla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                benzerverikontrol(ad, soyad, tc, refGonder);
            }
        });
        sil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hastayakinisil(refGonder);
            }
        });
    }
    private void benzerverikontrol(final String ad, final String soyad, final String tc, final DatabaseReference refGonder)
    {

        final DatabaseReference dbGelenler = db.getReference("hastayakini");
        dbGelenler.addListenerForSingleValueEvent(new ValueEventListener() {
            int sayac=0;
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot gelenler: dataSnapshot.getChildren()) {
                    if (gelenler.child("ad").getValue().toString().equals(ad) &&
                            gelenler.child("soyad").getValue().toString().equals(soyad) &&
                            gelenler.child("tc").getValue().toString().equals(tc)) {
                        Toast.makeText(getApplicationContext(), "böyle bir hasta yakini sistemde zaten mevcuttur!", Toast.LENGTH_LONG).show();
                    sayac++;
                    }
                }
                if(sayac==0){ yenihastayakiniekle(ad,soyad,tc,refGonder);}
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void yenihastayakiniekle(String ad, String soyad, String tc, DatabaseReference refGonder)
    {
        DatabaseReference dbRef = db.getReference("hastayakini");
        String key = dbRef.push().getKey();
        DatabaseReference dbRefKeyli = db.getReference("hastayakini/" + key);
        dbRefKeyli.setValue(new doktor(ad, soyad, tc));
        refGonder.removeValue();
        Intent refresh = new Intent(this, onaybekleyenler.class);
        startActivity(refresh);//Start the same Activity
        finish(); //finish Activity.
    }
    private void hastayakinisil(DatabaseReference refGonder)
    {
        refGonder.removeValue();
        Intent refresh = new Intent(this, onaybekleyenler.class);
        startActivity(refresh);//Start the same Activity
        finish(); //finish Activity.
    }


}