package com.example.sumeyra.bitirme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;

/**
 * Created by Sumeyra on 3.3.2017.
 */
public class giris extends Activity {
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.giris);
        Button doktor = (Button) findViewById(R.id.button2);
        doktor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(giris.this, doktorgiris.class);
                startActivity(i);
            }
        });
        Button hastayakini = (Button) findViewById(R.id.button3);
        hastayakini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(giris.this, hastayakini.class);
                startActivity(i);
            }
        });
    }
}

